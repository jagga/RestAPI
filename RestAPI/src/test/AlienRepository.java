package test;

import java.util.ArrayList;
import java.util.List;

public class AlienRepository {
	List<Alien> aliens;

	public AlienRepository() {
		aliens = new ArrayList<>();
		Alien a = new Alien();
		a.setName("jaga");
		a.setId(1);
		a.setPoints(20);
		Alien a1 = new Alien();
		a1.setName("Navin");
		a1.setId(2);
		a1.setPoints(70);
		aliens.add(a);
		aliens.add(a1);

	}

	public List<Alien> getAliens() {
		return aliens;
	}

	public Alien getAlien(int id) {
		for (Alien a : aliens) {
			if (a.getId() == id) {
				return a;
			}
		}
		return new Alien();

	}

	public void create(Alien a) {
		aliens.add(a);

	}

}
